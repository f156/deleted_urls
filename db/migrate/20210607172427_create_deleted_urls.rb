class CreateDeletedUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :deleted_urls do |t|
      t.string :prev_url, index: { unique: true }, null: false
      t.string :next_url, null: false
      t.datetime :created_at, default: 'now()', null: false
    end

    add_check_constraint :deleted_urls, 'prev_url <> next_url', name: :check_constraint_urls
  end
end
