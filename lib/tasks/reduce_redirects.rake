# frozen_string_literal: true

desc 'Reduce redirects'
task reduce_redirects: :environment do
  puts "start #{Time.zone.now}"

  max_jump = 10

  DeletedUrl.find_each do |deleted_url|
    parent = deleted_url.reload.parent

    next if parent.nil?

    count_jump = 1
    jump_ids = [deleted_url.id, parent.id]
    tail = parent

    loop do
      parent = tail.parent

      break if parent.nil?

      jump_ids << parent.id
      count_jump += 1
      tail = parent
    end

    DeletedUrl.where(id: jump_ids).update(next_url: tail.next_url) if count_jump > max_jump
  end

  puts "end #{Time.zone.now}"
end
