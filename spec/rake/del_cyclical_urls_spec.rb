# frozen_string_literal: true

Rails.application.load_tasks

describe 'del_cyclical_urls.rake' do
  before do
    DeletedUrl.create([
                        { prev_url: 'A', next_url: 'B' },
                        { prev_url: 'C', next_url: 'D' },
                        { prev_url: 'B', next_url: 'E' },
                        { prev_url: 'E', next_url: 'A' }
                      ])

    Rake::Task['del_cyclical_urls'].invoke
  end

  after { Rake::Task['del_cyclical_urls'].reenable }

  it { expect(DeletedUrl.count).to eq 3 }
  it { expect(DeletedUrl.last.prev_url).to eq 'E' }
  it { expect(DeletedUrl.first.prev_url).to eq 'C' }
end
