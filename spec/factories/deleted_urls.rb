# frozen_string_literal: true

FactoryBot.define do
  sequence(:prev_url) { |n| "url_#{n}" }
  sequence(:next_url) { |n| "url_#{n + 1}" }

  factory :deleted_url do
    prev_url
    next_url
  end
end
