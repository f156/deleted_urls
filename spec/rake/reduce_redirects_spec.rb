# frozen_string_literal: true

Rails.application.load_tasks

describe 'reduce_redirects.rake' do
  let!(:deleted_urls) { create_list(:deleted_url, 15) }

  before { Rake::Task['reduce_redirects'].invoke }
  after { Rake::Task['reduce_redirects'].reenable }

  it { expect(DeletedUrl.distinct.pluck(:next_url).size).to eq 1 }
  it { expect(DeletedUrl.distinct.pluck(:next_url)[0]).to eq deleted_urls.last.next_url }
  it { expect(DeletedUrl.first.next_url).to eq deleted_urls.last.next_url }
end
