# README

```bash
docker-compose build

docker-compose run --rm -e RAILS_ENV=development deleted_urls rails c

docker-compose run --rm -e RAILS_ENV=development deleted_urls rake del_cyclical_urls
docker-compose run --rm -e RAILS_ENV=development deleted_urls rake reduce_redirects
```
