# frozen_string_literal: true

desc 'Delete ciclyc urls'
task del_cyclical_urls: :environment do
  puts "start #{Time.zone.now}"

  DeletedUrl.find_each do |deleted_url|
    parent = deleted_url.reload.parent
    next if parent.nil?

    loop do
      if deleted_url.prev_url == parent.next_url
        deleted_url.delete
        break
      end

      parent = parent.parent

      break if parent.nil?
    end
  rescue ActiveRecord::RecordNotFound
    next
  end

  puts "end #{Time.zone.now}"
end
