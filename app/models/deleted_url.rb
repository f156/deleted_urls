# frozen_string_literal: true

class DeletedUrl < ApplicationRecord
  validates :prev_url, :next_url, presence: true
  validates :prev_url, uniqueness: true

  has_one :parent,
          required: false,
          class_name: 'DeletedUrl',
          primary_key: :next_url,
          foreign_key: :prev_url,
          inverse_of: false,
          dependent: false
end
